/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxfirstversion;

import java.util.Scanner;

/**
 *
 * @author BankMMT
 */
public class OXgameFirst {

    static int row = 0;
    static int col = 0;
    static int turnCount = 1;
    static String win = " ";
    static boolean checkWin = false;
    static boolean turn = false;
    static boolean checkDraw = false;
    static String[][] ox = new String[3][3];
    static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to The OX Game !! ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                ox[i][j] = "-";
            }
        }
        while (checkWin != true && checkDraw != true) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(ox[i][j] + ' ');
                }
                System.out.println();
            }
            if (!turn) {
                inputAlphabetToSlot("O");
            } else {
                inputAlphabetToSlot("X");
            }
            if (((ox[0][0].equals("X") && ox[0][1].equals("X")) && ox[0][2].equals("X")) || ((ox[0][0].equals("O") && ox[0][1].equals("O")) && ox[0][2].equals("O"))) {
                win = ox[0][0];
            } else if (((ox[1][0].equals("X") && ox[1][1].equals("X")) && ox[1][2].equals("X")) || ((ox[1][0].equals("O") && ox[1][1].equals("O")) && ox[1][2].equals("O"))) {
                win = ox[1][0];
            } else if (((ox[2][0].equals("X") && ox[2][1].equals("X")) && ox[2][2].equals("X")) || ((ox[2][0].equals("O") && ox[2][1].equals("O")) && ox[2][2].equals("O"))) {
                win = ox[2][0];
            } else if (((ox[0][0].equals("X") && ox[1][0].equals("X")) && ox[2][0].equals("X")) || ((ox[0][0].equals("O") && ox[1][0].equals("O")) && ox[2][0].equals("O"))) {
                win = ox[0][0];
            } else if (((ox[0][1].equals("X") && ox[1][1].equals("X")) && ox[2][1].equals("X")) || ((ox[0][1].equals("O") && ox[1][1].equals("O")) && ox[2][1].equals("O"))) {
                win = ox[0][1];
            } else if (((ox[0][2].equals("X") && ox[1][2].equals("X")) && ox[2][2].equals("X")) || ((ox[0][2].equals("O") && ox[1][2].equals("O")) && ox[2][2].equals("O"))) {
                win = ox[0][2];
            } else if (((ox[0][0].equals("X") && ox[1][1].equals("X")) && ox[2][2].equals("X")) || ((ox[0][0].equals("O") && ox[1][1].equals("O")) && ox[2][2].equals("O"))) {
                win = ox[0][0];
            } else if (((ox[0][2].equals("X") && ox[1][1].equals("X")) && ox[2][0].equals("X")) || ((ox[0][2].equals("O") && ox[1][1].equals("O")) && ox[2][0].equals("O"))) {
                win = ox[0][2];
            }
            checkWin();
            if (turnCount > 9) {
                checkDraw = true;
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(ox[i][j] + ' ');
            }
            System.out.println();
        }
        if (checkDraw) {
            System.out.println(">>>> Draw <<<<");
        } else {
            System.out.println(">>>> " + win + " win!! <<<<");
        }

    }

    public static void inputAlphabetToSlot(String alphabet) {
        System.out.println("It's Turn " + alphabet);
        System.out.println("Let's input row,col");
        row = kb.nextInt();
        col = kb.nextInt();
        if ((row >= 1 && row <= 3) && (col >= 1 && col <= 3)) {
            if (ox[row - 1][col - 1].equals("X") || ox[row - 1][col - 1].equals("O")) {
                System.out.println("This slot wasn't empty!!");
            } else {
                ox[row - 1][col - 1] = alphabet;
                if (alphabet.equals("O")) {
                    turn = true;
                } else {
                    turn = false;
                }
                turnCount++;
            }
        } else {
            System.out.println("Out of table !! Input again");
        }
    }

    public static void checkWin() {
        if (win.equals("X") || win.equals("O")) {
            checkWin = true;
        }
    }
}
